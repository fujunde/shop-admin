import dayjs from 'dayjs'
export default {
    dealDate:(val)=>{
        return dayjs(val).format('YYYY-MM-DD HH:mm:ss')
    },
    dealDate2:(val)=>{
        return dayjs(val).format('YYYY-MM-DD')
    }
}