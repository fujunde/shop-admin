import axios from 'axios'
import {Message} from 'element-ui'
import store from '../store/index'
import router from '../router'

let baseURL=""

// const baseURL="http://39.107.51.204:8889/api/private/v1/"
// axios.defaults.baseURL="https://pc.raz-kid.cn/api/private/v1/"
// axios.defaults.baseURL="http://118.190.39.123:8360/api/private/v1/"
if(process.env.NODE_ENV=="development"){ //开发环境
    baseURL="http://39.107.51.204:8889/api/private/v1/"
}else if(process.env.NODE_ENV=='test'){  //测试环境
    baseURL="http://39.107.51.204:8889/api/private/v1/"
}else if(process.env.NODE_ENV=='production'){  //生产环境
    baseURL="http://39.107.51.204:8889/api/private/v1/"
}
axios.defaults.baseURL=baseURL

axios.interceptors.response.use(function (response) {
    if(response.data.meta.msg=='无效token'){
        router.push({
            name:'login'
        })
    }
    return response;
  }, function (error) {
    return Promise.reject(error);
  });

const http =(url,method='get',data={},params)=>{
    return new Promise((resolve,reject)=>{
        axios({
            url,
            method:method.toLowerCase(),
            headers:{
                authorization:store.state.token
            },
            data,
            params
        }).then(res=>{
            if(res.status >= 200 && res.status <300 ||res.status===304){
                if(res.data.meta.status >=200 && res.data.meta.status<300){
                    resolve(res.data)
                }else{
                    Message.error(res.data.meta.msg)
                    reject(res)
                }
            }else{ 
                Message.error(res.statusText)
                reject(res)
            }
        }).catch(err=>{
            reject(err)
        })
    })
}
export default http
export const _baseURL = baseURL