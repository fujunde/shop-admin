import http from './index'
import {Message} from 'element-ui'

//添加用户
export function addUsers(data){
    return http('users','POST',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        console.log(res)
        return res.data
    })   
}

//修改用户状态
export function updateUserState(uId,type) {
    return http(`users/${uId}/state/${type}`,'PUT').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

//编辑用户提交
export function editUser(obj) {
    return http(`users/${obj.id}`,'PUT',{
        email:obj.email,
        mobile:obj.mobile
    }).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

//删除单个用户
export function deleteUser(id) {
    return http(`users/${id}`,'delete').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

//查询用户状态
export function allotUser(id) {
    return http(`users/${id}`,'GET').then(res=>{
        return res.data
    })
}

//根据角色id获取角色列表
export function GetRoleLists() {
    return http('roles','GET').then(res=>{
        return res.data
    })
}

//分配用户角色
export function allotRole(id,rid) {
    return http(`users/${id}/role`,"PUT",{
        rid
    }).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 添加角色
export function addRoles(data){
    return http('roles','POST',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 编辑提交角色
export function editRoles(id,data){
    return http(`roles/${id}`,'PUT',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 删除角色
export function deleteRoles(id){
    return http(`roles/${id}`,'DELETE').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 删除角色指定权限
export function deleteRight(roleId,rightId){
    return http(`roles/${roleId}/rights/${rightId}`,'DELETE').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 获取所有权限列表
export function allotLists(type){
    return http(`rights/${type}`,'GET').then(res=>{
        return res.data
    })
}


// 角色授权
export function roleAuthorization(roleId,rids){
    return http(`roles/${roleId}/rights`,'POST',{rids}).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 获取商品数据列表
export function goodsLists(data){
    return http('goods','GET',{},data).then(res=>{
        return res.data
    })
}

// 根据id查询商品
export function getGoodsInfo(id){
    return http(`goods/${id}`).then(res=>{
        return res.data
    })
}

// 编辑提交商品
export function editGoods(id,data){
    return http(`goods/${id}`,"PUT",data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

//删除商品
export function deleteGoods(id){
    return http(`goods/${id}`,'DELETE').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 商品数据列表
export function getGoodsCategories(type,pagenum,pagesize){
    return http('categories',"GET",{},{type,pagenum,pagesize}).then(res=>{
        return res.data
    })
}

//商品参数列表
export function getGoodsAttrs(id,sel){
    return http(`categories/${id}/attributes`,'GET',{},{sel}).then(res=>{
        return res.data
    })
}

// 添加商品
export function addGood(data){
    return http('goods',"POST",data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 编辑提交参数
export function editParams(id,attrId,data){
    return http(`categories/${id}/attributes/${attrId}`,'PUT',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}


// 删除参数
export function deleteParams(id,attrId){
    return http(`categories/${id}/attributes/${attrId}`,'delete').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 添加动态参数或者静态参数
export function setParams(id,data){
    return http(`categories/${id}/attributes`,'POST',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 添加分类
export function setClassify(data){
    return http('categories',"POST",data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 编辑提交分类
export function editClassify(id,data){
    return http(`categories/${id}`,'PUT',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 删除分类
export function deleteClassify(id){
    return http(`categories/${id}`,'DELETE').then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 订单数据列表
export function getOrders(data){
    return http('orders','GET',{},data).then(res=>{
        return res.data
    })
}

// 修改订单状态
export function editOrders(id,data){
    return http(`orders/${id}`,'PUT',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })
}

// 数据统计：基于时间统计（折线图）
export function getReports(type){
    return http(`reports/type/${type}`).then(res=>{
        return res.data
    })
}