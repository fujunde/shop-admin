import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

Vue.use(VueRouter)
  const routes = [
  {
    path: '/',
    redirect:"/login"
  },
  {
    path:"/login",
    name:"login",
    component:Login
  },
  {
    path:'/index',
    name:"index",
    redirect:"/index/users",
    component:()=>import('../views/Index.vue'),
    children:[
      {
        path:'users',
        name:"users",
        component:()=>import('../views/Users.vue'),
      },
      {
        path:'roles',
        name:"roles",
        component:()=>import('../views/rights/Roles.vue'),
      },
      {
        path:'rights',
        name:"rights",
        component:()=>import('../views/rights/Rights.vue'),
      },
      {
        path:'goods',
        name:"goods",
        component:()=>import('../views/goods/Goods.vue'),
      },
      {
        path:'goodsadd',
        name:"goodsadd",
        component:()=>import('../views/goods/GoodsAdd.vue'),
      },
      {
        path:'goodsedit/:id',
        name:"goodsedit",
        component:()=>import('../views/goods/GoodsAdd.vue'),
        props:true
      },
      {
        path:'params',
        name:"params",
        component:()=>import('../views/goods/GoodsParams.vue'),
      },
      {
        path:'categories',
        name:"categories",
        component:()=>import('../views/goods/GoodsCat.vue'),
      },
      {
        path:'orders',
        name:"orders",
        component:()=>import('../views/Orders.vue'),
      },
      {
        path:'reports',
        name:"reports",
        component:()=>import('../views/Reports.vue'),
      },
      
    ]
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
