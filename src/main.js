import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import "reset-css"
// import http from '@/http'
import http from '@/plugins'

import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
  
Vue.use(VueQuillEditor)

import filters from "@/filters"
Object.keys(filters).forEach(k=>{
  Vue.filter(k,filters[k])
})
Vue.use(ElementUI);

Vue.config.productionTip = false

// Vue.prototype.$http=http
Vue.use(http);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

